﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Angular4Asp.NetWebForms.Startup))]
namespace Angular4Asp.NetWebForms
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
