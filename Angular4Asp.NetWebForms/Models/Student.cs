﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIWithWebForm.Models
{
    public class Students
    {
        public int StudRollNO { get; set; }
        public string StudName { get; set; }
        public string StudAddress { get; set; }
        public int StudMONO { get; set; }
        public string StudCourse { get; set; }
    }

    public class Teacher
    {
        public int TeacherId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}