﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using WebAPIWithWebForm.Models;
using static WebAPIWithWebForm.Student;

namespace WebAPIWithWebForm
{
    public class CollegeController : ApiController
    {
        public static List<Teacher> GetTeachers()
        {
            List<Teacher> teachers = new List<Teacher>();
            teachers.Add(new Teacher { TeacherId = 1, Code = "TT", Name = "Tejas Trivedi" });
            teachers.Add(new Teacher { TeacherId = 2, Code = "JT", Name = "Jignesh Trivedi" });
            teachers.Add(new Teacher { TeacherId = 3, Code = "RT", Name = "Rakesh Trivedi" });
            return teachers;
        }
    
        [HttpGet]
        public static List<Students> GetStudents()
        {
            List<Students> students = new List<Students>();
            students.Add(new Students { StudRollNO = 1, StudName = "Yogendra", StudAddress = "Noida", StudMONO = 123, StudCourse = "BTech" });
            students.Add(new Students { StudRollNO = 2, StudName = "Shubh", StudAddress = "Delhi", StudMONO = 456, StudCourse = "MTech" });
            students.Add(new Students { StudRollNO = 3, StudName = "Santesh", StudAddress = "Lucknow", StudMONO = 789, StudCourse = "MBA" });
            return students;
        }
    }
}