﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAPIWithWebForm
{
    public partial class Student : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var StudentList = CollegeController.GetStudents();
            repStudentList.DataSource = StudentList;
            repStudentList.DataBind();
        }
    }
}