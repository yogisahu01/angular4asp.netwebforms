﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="WebAPIWithWebForm.Student" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
</head>

<body>
    <a href="../AngularApp/src/app/Student/Student.Component.html">Student.html from WebForm</a>
    <asp:Repeater ID="repStudentList" runat="server">
        <HeaderTemplate>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div class="header_container">
                                <div class="header_title">StudName</div>
                                <div class="header_title">StudAddress</div>
                                <div class="header_title">StudMONO</div>
                                <div class="header_title">StudCourse</div>
                            </div>
                        </td>
                    </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <div class="title_container">
                        <div class="row_detail"><%# Eval("StudName") %></div>
                        <div class="row_detail"><%# Eval("StudAddress") %></div>
                        <div class="row_detail"><%# Eval("StudMONO") %></div>
                        <div class="row_detail"><%# Eval("StudCourse") %></div>
                    </div>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
    </table>
        </FooterTemplate>
    </asp:Repeater>
   
</body>
</html>
